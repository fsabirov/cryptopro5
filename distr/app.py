#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename

import sys, os, base64
sys.path.append(r'/opt/cprocsp/lib/amd64')
import pycades

UPLOAD_FOLDER = '/tmp/'
ALLOWED_EXTENSIONS = set(['doc', 'docx', 'txt', 'pdf', 'png', 'jpg', 'jpeg', 'sig'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return ("Cryptopro")


@app.route('/sign', methods=['GET', 'POST'])
def sign():
    thumb = request.form.get('thumb')

    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filepath = UPLOAD_FOLDER+filename

        # Signing
        store = pycades.Store()
        store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE, pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
        certs = store.Certificates
        assert (certs.Count != 0), "Certificates with private key not found"

        certs = certs.Find(pycades.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, thumb)

        signer = pycades.Signer()
        signer.Certificate = certs.Item(1)
        signer.CheckCertificate = True

        with open(filepath, 'rb') as ff:
            content = base64.b64encode(ff.read()).decode()
        ff.close()

        signedData = pycades.SignedData()
        signedData.ContentEncoding = pycades.CADESCOM_BASE64_TO_BINARY
        signedData.Content = content
        signature = signedData.SignCades(signer, pycades.CADESCOM_CADES_BES, True, pycades.CADESCOM_STRING_TO_UCS2LE)

        return jsonify({'signature': signature})

@app.route('/verify', methods=['GET', 'POST'])
def verify():
    if request.method == 'POST':

        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filepath = UPLOAD_FOLDER + filename

        file = request.files['signature']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            signaturepath = UPLOAD_FOLDER + filename

        with open(filepath, 'rb') as ff:
            content = base64.b64encode(ff.read()).decode()
        ff.close()

        with open(signaturepath, 'rt') as ff:
            signature = ff.read()
        ff.close()

        _signedData = pycades.SignedData()
        _signedData.ContentEncoding = pycades.CADESCOM_BASE64_TO_BINARY
        _signedData.Content = content
        try:
            _signedData.VerifyCades(signature, pycades.CADESCOM_CADES_BES, True)
            cert = _signedData.Certificates.Item(1)
            crt = {'Valid': True,
                   'IssuerName': cert.IssuerName,
                   'SerialNumber': cert.SerialNumber,
                   'Thumbprint': cert.Thumbprint,
                    'SubjectName': cert.SubjectName,
                    'ValidFromDate': cert.ValidFromDate,
                    'ValidToDate': cert.ValidToDate
                   }
            return jsonify(crt)
        except:
            return jsonify({'Valid': False})


@app.route('/certificates', methods=['GET'])
def certificates():

    store = pycades.Store()
    store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE, pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
    certs = store.Certificates
    if (certs.Count == 0):
        return 'Certificates not found!'
    else:
        lists=[]
        for i in range(1, certs.Count+1):
            cert=certs.Item(i)
            crt={'HasPrivateKey': cert.HasPrivateKey(),
                 'IssuerName': cert.IssuerName,
                 'SerialNumber': cert.SerialNumber,
                 'Thumbprint': cert.Thumbprint,
                 'SubjectName': cert.SubjectName,
                 'ValidFromDate': cert.ValidFromDate,
                 'ValidToDate': cert.ValidToDate,
                 'Version': cert.Version
                 }
            lists.append(crt)
        return jsonify(lists)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8080', debug=True)
