FROM debian:buster-slim
MAINTAINER Fanis Sabirov <fanis.sabirov@agropoliya.ru>

ENV LANG C.UTF-8 
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND noninteractive

COPY ./distr/* /tmp/cryptopro/

RUN apt-get update && apt-get install --no-install-recommends -y cmake build-essential libboost-all-dev unzip \
	python3-dev python3-pip libssl-dev libffi-dev python3-venv \
	&& rm /etc/localtime \
	&& ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
	&& cd /tmp/cryptopro \
	&& tar xf linux-amd64_deb.tgz \
	&& ./linux-amd64_deb/install.sh \
	&& apt-get install -y ./linux-amd64_deb/lsb-cprocsp-devel_5.0*.deb \
	&& tar xf cades_linux_amd64.tar.gz \
	&& apt-get install -y ./cades_linux_amd64/cprocsp-pki-cades*.deb \
	&& unzip pycades.zip \
	&& cd pycades_* \
	&& sed -i 's#SET(Python_INCLUDE_DIR "/usr/include/python3.8")#SET(Python_INCLUDE_DIR "/usr/include/python3.7")#g' CMakeLists.txt \
	&& mkdir build \
	&& cd build \
	&& cmake .. \
	&& make -j4 \
	&& cp ./pycades.so /opt/cprocsp/lib/amd64/ \
	&& mkdir -p /opt/cprocsp-app \
	&& cp /tmp/cryptopro/app.py /opt/cprocsp-app/ \
	&& chmod +x /opt/cprocsp-app/app.py \
	&& pip3 install -r /tmp/cryptopro/requirements.txt \
	&& echo "export PATH=${PATH}:/opt/cprocsp/bin/amd64:/opt/cprocsp/sbin/amd64" >> ~/.bashrc \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 8080
WORKDIR /opt/cprocsp-app
CMD ["python3", "app.py"]
